package com.example.r42;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SuchController {

	@Value("${suchname}") private String suchName;

	AvailZoneFetcherService availZoneFetcherService;

	@Autowired
	public void setAvailZoneFetcherService(AvailZoneFetcherService availZoneFetcherService) {
		this.availZoneFetcherService = availZoneFetcherService;
	}


	@RequestMapping("/hello")
	public ResponseEntity<String> suchHello(){

		return new ResponseEntity<>("Hello ".concat(suchName).concat(" APP: optest"), HttpStatus.OK);

	}

	@RequestMapping("/time-stamp")
	public ResponseEntity<String> getTimestamp(){

		String az_zone = availZoneFetcherService.availZone();

		return new ResponseEntity<>("Hello ".concat(suchName).concat(" APP: timestamp , Time-Stamp and Availablity-Zone :   ").concat(az_zone), HttpStatus.OK);

	}
}
