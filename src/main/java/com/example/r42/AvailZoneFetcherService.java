package com.example.r42;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @Author vardamod on 05/07/20 00:33
 * @Projectname suchapp
 */
@Service
public class AvailZoneFetcherService {

    @Value("${availzone}") private String url;



    public String availZone() {

        RestTemplate restTemplate = new RestTemplate();

        return restTemplate.getForObject(url,String.class);

    }
}
